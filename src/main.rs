use hanoi::TowerSet;

fn main() {
    for x in 1..6 {
        let mut towers = TowerSet::new(x);

        towers.solve();
    }
}
