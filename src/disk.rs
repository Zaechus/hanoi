pub struct Disk {
    size: usize,
}

impl Disk {
    pub fn new(size: usize) -> Self {
        Self { size }
    }

    pub fn size(&self) -> usize {
        self.size
    }
}
