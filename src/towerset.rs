use crate::disk::Disk;

pub struct TowerSet {
    disk_num: usize,
    pegs: Vec<Vec<Disk>>,
    last_moved: usize,
}

impl TowerSet {
    pub fn new(disk_num: usize) -> Self {
        let mut pegs = vec![
            Vec::with_capacity(disk_num),
            Vec::with_capacity(disk_num - 1),
            Vec::with_capacity(disk_num),
        ];
        for size in (1..=disk_num).rev() {
            pegs[0].push(Disk::new(size));
        }
        Self {
            disk_num,
            pegs: pegs,
            last_moved: 4,
        }
    }

    fn move_disk(&mut self, start_peg: usize, end_peg: usize) -> bool {
        if let Some(disk) = self.pegs[start_peg].pop() {
            self.pegs[end_peg].push(disk);
            self.last_moved = end_peg;
            true
        } else {
            false
        }
    }

    fn check_move(
        &mut self,
        moving_disk_size: usize,
        moving_peg: usize,
        checked_peg1: usize,
        checked_peg2: usize,
    ) -> bool {
        if let Some(disk) = self.pegs[checked_peg1].last() {
            if moving_disk_size < disk.size() && moving_disk_size & 1 != disk.size() & 1 {
                return self.move_disk(moving_peg, checked_peg1);
            }
        }
        if let Some(disk) = self.pegs[checked_peg2].last() {
            if moving_disk_size < disk.size() && moving_disk_size & 1 != disk.size() & 1 {
                return self.move_disk(moving_peg, checked_peg2);
            }
        }
        if self.pegs[checked_peg1].is_empty() {
            return self.move_disk(moving_peg, checked_peg1);
        }
        if self.pegs[checked_peg2].is_empty() {
            return self.move_disk(moving_peg, checked_peg2);
        }
        false
    }

    fn can_move(&mut self, peg: usize) -> bool {
        let disk_size = if let Some(disk) = self.pegs[peg].last() {
            disk.size()
        } else {
            return false;
        };
        match peg {
            0 => self.check_move(disk_size, peg, 2, 1),
            1 => self.check_move(disk_size, peg, 0, 2),
            2 => self.check_move(disk_size, peg, 1, 0),
            _ => false,
        }
    }

    fn move_once(&mut self) {
        self.print_towers();
        if self.last_moved != 0 && self.can_move(0) {
        } else if self.last_moved != 1 && self.can_move(1) {
        } else if self.last_moved != 2 && self.can_move(2) {
        }
        println!("=====================");
    }

    pub fn solve(&mut self) {
        self.print_towers();
        println!("=====================");
        if self.disk_num & 1 == 1 {
            self.move_disk(0, 2);
        } else {
            self.move_disk(0, 1);
        }

        let mut moves = 1;
        while !self.pegs[0].is_empty() || !self.pegs[1].is_empty() {
            self.move_once();
            moves += 1;
        }
        self.print_towers();
        println!("=====================\nmoves: {}", moves);
    }

    fn print_towers(&self) {
        for peg in self.pegs.iter() {
            for disk in peg {
                print!("{} ", disk.size());
            }
            println!();
        }
    }
}
